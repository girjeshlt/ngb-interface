package com.mppkvvcl.ngbinterface.interfaces;

/**
 * Created by ASHISH on 12/14/2017.
 */
public interface MeterReaderInformationInterface extends BeanInterface {
    public long getId();

    public void setId(long id);

    public long getZoneId();

    public void setZoneId(long zoneId);

    public long getReadingDiaryNoId();

    public void setReadingDiaryNoId(long readingDiaryNoId);

    public String getName();

    public void setName(String name);

    public String getMobileNo();

    public void setMobileNo(String mobileNo);
}
