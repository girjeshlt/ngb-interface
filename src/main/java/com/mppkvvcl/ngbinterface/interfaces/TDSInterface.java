package com.mppkvvcl.ngbinterface.interfaces;

import java.math.BigDecimal;

public interface TDSInterface extends BeanInterface {

    public long getId();

    public void setId(long id);

    public long getSecurityDepositInterestId();

    public void setSecurityDepositInterestId(long securityDepositInterestId);

    public BigDecimal getAmount();

    public void setAmount(BigDecimal amount);
}
