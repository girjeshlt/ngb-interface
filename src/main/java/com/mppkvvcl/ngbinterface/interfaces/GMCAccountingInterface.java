package com.mppkvvcl.ngbinterface.interfaces;

import java.math.BigDecimal;

/**
 * Created by PREETESH on 11/7/2017.
 */
public interface GMCAccountingInterface extends BeanInterface {
    public long getId();

    public void setId(long id);

    public String getConsumerNo();

    public void setConsumerNo(String consumerNo);

    public String getCurrentBillMonth();

    public void setCurrentBillMonth(String currentBillMonth);

    public String getReadType();

    public void setReadType(String readType);

    public BigDecimal getCurrentConsumption();

    public void setCurrentConsumption(BigDecimal currentConsumption);

    public BigDecimal getActualCumulativeConsumption();

    public void setActualCumulativeConsumption(BigDecimal actualCumulativeConsumption);

    public BigDecimal getMinimumCumulative();

    public void setMinimumCumulative(BigDecimal minimumCumulative);

    public BigDecimal getHigherOfActualMinimumCumulative();

    public void setHigherOfActualMinimumCumulative(BigDecimal higherOfActualMinimumCumulative);

    public BigDecimal getAlreadyBilled();

    public void setAlreadyBilled(BigDecimal alreadyBilled);

    public BigDecimal getToBeBilled();

    public void setToBeBilled(BigDecimal toBeBilled);

    public String getPreviousMonth();

    public void setPreviousMonth(String previousMonth);

    public String getPreviousReadType();

    public void setPreviousReadType(String previousReadType);

    public BigDecimal getPreviousConsumption();

    public void setPreviousConsumption(BigDecimal previousConsumption);

    public BigDecimal getPreviousActualCumulativeConsumption();

    public void setPreviousActualCumulativeConsumption(BigDecimal previousActualCumulativeConsumption);

    public BigDecimal getPreviousMinimumCumulative();

    public void setPreviousMinimumCumulative(BigDecimal previousMinimumCumulative);

    public BigDecimal getPreviousHigherOfActualMinimumCumulative();

    public void setPreviousHigherOfActualMinimumCumulative(BigDecimal previousHigherOfActualMinimumCumulative);

    public BigDecimal getPreviousAlreadyBilled();

    public void setPreviousAlreadyBilled(BigDecimal previousAlreadyBilled);

    public BigDecimal getPreviousToBeBilled();

    public void setPreviousToBeBilled(BigDecimal previousToBeBilled);

}
