package com.mppkvvcl.ngbinterface.interfaces;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by RUPALI on 9/22/2017.
 */
public interface ConsumerConnectionMeterInformationInterface extends BeanInterface {

    public static final String HAS_CTR = "Y";

    public static final String HAS_MODEM = "Y";

    public long getId();

    public void setId(long id);

    public String getConsumerNo();

    public void setConsumerNo(String consumerNo);

    public String getMeterIdentifier();

    public void setMeterIdentifier(String meterIdentifier);

    public BigDecimal getStartRead();

    public void setStartRead(BigDecimal startRead);

    public boolean getHasCTR();

    public void setHasCTR(boolean hasCTR);

    public Date getMeterInstallationDate();

    public void setMeterInstallationDate(Date meterInstallationDate);

    public String getMeterInstallerName();

    public void setMeterInstallerName(String meterInstallerName);

    public String getMeterInstallerDesignation();

    public void setMeterInstallerDesignation(String meterInstallerDesignation);

    public boolean getHasModem();

    public void setHasModem(boolean hasModem);

    public Date getCreatedOn();

    public void setCreatedOn(Date createdOn);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy);

    public Date getUpdatedOn();

    public void setUpdatedOn(Date updatedOn);

    public String getUpdatedBy();

    public void setUpdatedBy(String updatedBy);


}
