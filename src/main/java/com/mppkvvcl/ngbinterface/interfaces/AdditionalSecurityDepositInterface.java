package com.mppkvvcl.ngbinterface.interfaces;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by PREETESH on 11/17/2017.
 */
public interface AdditionalSecurityDepositInterface extends BeanInterface {
    public long getId();

    public void setId(long id);

    public String getConsumerNo();

    public void setConsumerNo(String consumerNo);

    public BigDecimal getAverageConsumption();
    public void setAverageConsumption(BigDecimal averageConsumption);

    public BigDecimal getAverageBill();

    public void setAverageBill(BigDecimal averageBill);

    public boolean isDefaulter();

    public void setDefaulter(boolean defaulter);

    public String getBillMonth();

    public void setBillMonth(String billMonth);

    public BigDecimal getSecurityDepositDemand();

    public void setSecurityDepositDemand(BigDecimal securityDepositDemand);

    public int getPeriod();

    public void setPeriod(int period);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy);

    public Date getCreatedOn();

    public void setCreatedOn(Date createdOn);

    public String getUpdatedBy();

    public void setUpdatedBy(String updatedBy);

    public Date getUpdatedOn();

    public void setUpdatedOn(Date updatedOn);

}
