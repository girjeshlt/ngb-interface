package com.mppkvvcl.ngbinterface.interfaces;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by PREETESH on 12/18/2017.
 */
public interface XrayConnectionFixedChargeRateInterface extends BeanInterface{

    public static final String XRAY_TYPE_SINGLE = "SINGLE";
    public static final String XRAY_TYPE_THREE = "THREE";
    public static final String XRAY_TYPE_DENTAL_SINGLE = "DENTAL_SINGLE";

    public long getId();

    public void setId(long id);

    public long getTariffId();

    public void setTariffId(long tariffId);

    public long getSubcategoryCode();

    public void setSubcategoryCode(long subcategoryCode);

    public String getXrayType();

    public void setXrayType(String xrayType);

    public String getRate();

    public void setRate(String rate);

    public BigDecimal getMultiplier();

    public void setMultiplier(BigDecimal multiplier);

    public Date getCreatedOn();

    public void setCreatedOn(Date createdOn);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy);
}
