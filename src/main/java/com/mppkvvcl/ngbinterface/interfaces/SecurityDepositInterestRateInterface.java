package com.mppkvvcl.ngbinterface.interfaces;

import java.math.BigDecimal;
import java.util.Date;

public interface SecurityDepositInterestRateInterface extends BeanInterface {

    public long getId();

    public void setId(long id);

    public String getRate();

    public void setRate(String rate);

    public BigDecimal getMultiplier();

    public void setMultiplier(BigDecimal multiplier);

    public BigDecimal getThreshold();

    public void setThreshold(BigDecimal threshold);

    public String getTdsRateOne();

    public void setTdsRateOne(String tdsRateOne);

    public BigDecimal getTdsRateOneMultiplier();

    public void setTdsRateOneMultiplier(BigDecimal tdsRateOneMultiplier);

    public String getTdsRateTwo();

    public void setTdsRateTwo(String tdsRateTwo);

    public BigDecimal getTdsRateTwoMultiplier();

    public void setTdsRateTwoMultiplier(BigDecimal tdsRateTwoMultiplier);

    public Date getEffectiveStartDate();

    public void setEffectiveStartDate(Date effectiveStartDate);

    public Date getEffectiveEndDate();

    public void setEffectiveEndDate(Date effectiveEndDate);

    public String getOrderReference();

    public void setOrderReference(String orderReference);
}
