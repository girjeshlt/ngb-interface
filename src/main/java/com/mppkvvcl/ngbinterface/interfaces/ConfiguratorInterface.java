package com.mppkvvcl.ngbinterface.interfaces;

/**
 * Created by RUPALI on 9/22/2017.
 */
public interface ConfiguratorInterface extends BeanInterface {

    public static final String RC_DC_CHARGES = "RC-DC-CHARGES";

    public static final String RC_DC_CODE = "RC-DC-CODE";

    public static final String BANK_CHARGES_CODE = "BANK-CHARGES-CODE";

    public static final String PREV_ARREAR_CODE = "PREV-ARREAR-CODE";

    public static final String BANK_CHARGES = "BANK-CHARGE";

    public static final String AGREEMENT_PERIOD = "AGREEMENT-PERIOD";

    public static final String RC_DC = "RC-DC";

    public static final String NOT_ACCEPTING_CHEQUE_AFTER_DISHONOUR = "NOT-ACCEPTING-CHEQUE-AFTER-DISHONOUR";

    public static final String PART_PAYMENT_VALID_FOR_DAYS = "PART-PAYMENT-VALID-FOR-DAYS";

    public long getId();

    public void setId(long id);

    public String getCode();

    public void setCode(String code);

    public long getValue();

    public void setValue(long value);

}
