package com.mppkvvcl.ngbinterface.interfaces;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by RUPALI on 9/22/2017.
 */
public interface ConsumerConnectionInformationInterface extends BeanInterface{

    public static final String PHASE_SINGLE = "SINGLE";

    public static final String PHASE_THREE = "THREE";

    public static final String METERING_STATUS_METERED = "METERED";

    public static final String METERING_STATUS_UNMETERED = "UNMETERED";

    public static final String IS_SEASONAL = "Y";

    public static final String IS_GOVERNMENT = "Y";

    public static final String  IS_XRAY = "Y";

    public static final String CONNECTION_TYPE_TEMPORARY = "TEMPORARY";

    public long getId();

    public void setId(long id);

    public String getConsumerNo();

    public void setConsumerNo(String consumerNo);

    public String getTariffCategory();

    public void setTariffCategory(String tariffCategory);

    public String getConnectionType();

    public void setConnectionType(String connectionType);

    public String getMeteringStatus();

    public void setMeteringStatus(String meteringStatus);

    public String getPremiseType();

    public void setPremiseType(String premiseType);

    public BigDecimal getSanctionedLoad();

    public void setSanctionedLoad(BigDecimal sanctionedLoad);

    public String getSanctionedLoadUnit();

    public void setSanctionedLoadUnit(String sanctionedLoadUnit);

    public BigDecimal getContractDemand();

    public void setContractDemand(BigDecimal contractDemand);

    public String getContractDemandUnit();

    public void setContractDemandUnit(String contractDemandUnit);

    public boolean getIsSeasonal();

    public void setIsSeasonal(boolean isSeasonal);

    public Long getPurposeOfInstallationId();

    public void setPurposeOfInstallationId(Long purposeOfInstallationId);

    public String getPurposeOfInstallation();

    public void setPurposeOfInstallation(String purposeOfInstallation);

    public String getTariffCode();

    public void setTariffCode(String tariffCode);

    public long getSubCategoryCode();

    public void setSubCategoryCode(long subCategoryCode);

    public String getPhase();

    public void setPhase(String phase);

    public boolean getIsGovernment();

    public void setIsGovernment(boolean isGovernment);

    public BigDecimal getPlotSize();

    public void setPlotSize(BigDecimal plotSize);

    public String getPlotSizeUnit();

    public void setPlotSizeUnit(String plotSizeUnit);

    public String getLocationCode();

    public void setLocationCode(String locationCode);

    public boolean getIsXray();

    public void setIsXray(boolean isXray);

    public boolean getIsWeldingTransformerSurcharge();

    public void setIsWeldingTransformerSurcharge(boolean isWeldingTransformerSurcharge);

    public boolean getIsCapacitorSurcharge();

    public void setIsCapacitorSurcharge(boolean isCapacitorSurcharge) ;

    public boolean getIsDemandside();

    public void setIsDemandside(boolean isDemandside);

    public boolean getIsBeneficiary();

    public void setIsBeneficiary(boolean isBeneficiary);

    public Date getConnectionDate();

    public void setConnectionDate(Date connectionDate);

    public Date getCreatedOn();

    public void setCreatedOn(Date createdOn);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy);

    public Date getUpdatedOn();

    public void setUpdatedOn(Date updatedOn);

    public String getUpdatedBy();

    public void setUpdatedBy(String updatedBy);


}
