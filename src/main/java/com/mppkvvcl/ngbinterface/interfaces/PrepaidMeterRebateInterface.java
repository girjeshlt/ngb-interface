package com.mppkvvcl.ngbinterface.interfaces;

import java.math.BigDecimal;
import java.util.Date;

public interface PrepaidMeterRebateInterface extends BeanInterface {

    public long getId();

    public void setId(long id);

    public long getStartConsumption();

    public void setStartConsumption(long startConsumption);

    public long getEndConsumption();

    public void setEndConsumption(long endConsumption);

    public String getRate();

    public void setRate(String rate);

    public BigDecimal getMultiplier();

    public void setMultiplier(BigDecimal multiplier);

    public Date getEffectiveStartDate();

    public void setEffectiveStartDate(Date effectiveStartDate);

    public Date getEffectiveEndDate();

    public void setEffectiveEndDate(Date effectiveEndDate);

}
